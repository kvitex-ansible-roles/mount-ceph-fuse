Role Name
=========

Mounts cephfs as FUSE.
Makes fstab record fo permanent mount&

Requirements
------------
Requires installed ceph, ceph-fuse, ceph client keyring file and ceph coniguration file with list of monitors.


Role Variables
--------------



Dependencies
------------

To fullfill requirements you can use th following roles
ceph-common
ceph-fuse
ceph-deploy-keyring


Example Playbook
----------------


License
-------

BSD

Author Information
------------------

by Kuryanov Victor
